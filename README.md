# repeating digits in primes

calculate number of digits in the repeating pattern of an inverted prime (1/n)


--

## how it works

1. get a prime from the user.
2. get a power of 10 larger than that prime. example: prime 87, the next power is 100. call this the "dividend".
3. divide the "dividend" by the prime and round down to get a "quotient".
4. take the remainder of that division and call it "remainder".
5. if this combination of quotient and remainder has already been noted, show how many times this loop has been run and then exit.
6. if note the quotient and remainder combination. (this only has to be done the first time)
7. set "dividend" to the value of "remainder" and go back to step 3

--

## usage

to call where 89 is the prime:

```
php prime.php 89
```
