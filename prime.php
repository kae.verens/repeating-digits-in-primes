#!/usr/bin/php
<?php
$f=intval($argv[1]);
$base=intval($argv[2]??10);

$quot_mod='';
$length=0;
$val=pow($base, strlen($argv[1]));

while(1) {
	$mod=$val%$f;
	$quot=floor($val/$f);
	if ($quot_mod==$quot.'_'.$mod) {
		echo $length."\n";
		exit;
	}
	if ($quot_mod=='') {
		$quot_mod=$quot.'_'.$mod;
	}
	$length++;
	$val=$mod*$base;
}
